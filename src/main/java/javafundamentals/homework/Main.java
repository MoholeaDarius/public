package javafundamentals.homework;

import javafundamentals.homework.service.Service;
import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.repository.RepoImpl;
import javafundamentals.homework.view.CarMarketsComponent;

import static javafundamentals.homework.view.util.PrintTool.printDummyDataMenu;
import static javafundamentals.homework.view.util.Scanner.scan;
import static javafundamentals.homework.repository.DataGenerator.generateDummyData;


/**
 * <p>
 * - taote masinile grupate dupa tipul combustibilului folosit !!!!
 * - toate masinile care au numarul de km mai mic decat o anumita valoare citita de la tastatura !!!
 * <p>
 */
public class Main {

    private static Service service;

    public static void main(String[] args) {
        service = new Service(new RepoImpl());
        printDummyDataMenu();
        switch (scan.nextInt()) {
            case 1:
                addDummyData();
                break;
            case 0:
                return;
        }
        CarMarketsComponent carMarketsComponent = new CarMarketsComponent(service);
        carMarketsComponent.carMarketMenu();
    }

    private static void addDummyData() {
        for (CarMarket carMarket : generateDummyData()) {
            service.addCarMarket(carMarket.getName(), carMarket.getParkingCapacity());
            for (Car car : carMarket.getCarList()) {
                service.addCarToCarMarket(carMarket.getId(), car.getBrand(), car.getModel(), car.getFabricationYear(),
                        car.getCarType(), car.getMileage(), car.getHorsePower(), car.getTankVolume(), car.getFuelLevel(),
                        car.getFuelConsumptionPer100Km(), car.getInitialPrice());
            }
        }
    }

}
