package javafundamentals.homework.view;

import javafundamentals.homework.service.Service;

import static javafundamentals.homework.view.util.PrintTool.*;
import static javafundamentals.homework.view.util.Scanner.scan;

public class CarMarketsComponent {

    private final Service service;

    public CarMarketsComponent(Service service) {
        this.service = service;
    }

    public void carMarketMenu() {
        while (true) {
            printCarMarketsMenu();
            switch (scan.nextInt()) {
                case 1:
                    chooseAMarket();
                    break;
                case 2:
                    addNewMarket();
                    break;
                case 3:
                    removeMarket();
                    break;
                case 0:
                    return;
            }
        }
    }

    private void chooseAMarket() {
        printAllMarkets(service.getCarMarkets());
        System.out.print("Choose a market by id.\n" +
                "0. Back.\n" +
                "Option = ");
        int option = scan.nextInt();
        if (option != 0) {
            CarMarketComponent carMarketComponent = new CarMarketComponent(service, service.findCarMarketById(option));
            carMarketComponent.carMenu();
        }
    }

    private void addNewMarket() {
        System.out.print("Name = ");
        String name = scan.next();

        System.out.print("Capacity = ");
        int capacity = scan.nextInt();

        service.addCarMarket(name, capacity);
        System.out.println("Done!");
    }

    private void removeMarket() {
        printAllMarkets(service.getCarMarkets());
        System.out.print("Choose a market by id = ");
        int marketId = scan.nextInt();
        service.deleteCarMarket(marketId);
        System.out.println("Done!");
    }

}
