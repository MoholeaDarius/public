package javafundamentals.homework.view;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.constants.Status;
import javafundamentals.homework.service.Service;

import static javafundamentals.homework.view.util.Scanner.scan;
import static javafundamentals.homework.view.util.PrintTool.*;

public class CarComponents {

    private final Service service;
    private final int carMarketId;
    private final Car car;

    public CarComponents(Service service, int carMarketId, Car car) {
        this.service = service;
        this.carMarketId = carMarketId;
        this.car = car;
    }

    public void carMenu() {
        while (true) {
            printCarMenu(car.getBrand(), car.getModel());
            switch (scan.nextInt()) {
                case 1:
                    System.out.println(car);
                    break;
                case 2:
                    refill();
                    break;
                case 3:
                    testDrive();
                    break;
                case 4:
                    if (buyTheCar()) {
                        return;
                    }
                case 0:
                    return;
            }
        }
    }

    private void refill() {
        System.out.print("How manny liters you you want to put?\nLiters = ");
        double liters = scan.nextDouble();
        boolean wasFueled = car.fillTank(liters);
        if (wasFueled) {
            System.out.println("Done!");
        } else {
            System.out.println("Unable to refill. The available volume is: " + (car.getTankVolume() - car.getFuelLevel()) + "L, and you want to add: " + liters + "L.");
        }
    }

    private void testDrive() {
        System.out.print("How many kilometers do you want to drive?\nDistance = ");
        double distance = scan.nextDouble();
        if (car.getFuelLevel() == 0) {
            printRefillMenu();
            if (scan.nextInt() == 1) {
                refill();
            } else return;
        }
        double untraveledDistance = car.drive(distance);
        do {
            untraveledDistance = car.drive(untraveledDistance);
            if (untraveledDistance == 0) {
                System.out.println("Done!");
            } else {
                printNoMoreFuelMenu(untraveledDistance);
                int option = scan.nextInt();
                if (option == 1) {
                    car.fillTank(Math.min((untraveledDistance * car.getFuelConsumptionPer100Km() / 100), car.getTankVolume()));
                } else return;
            }
        } while (untraveledDistance != 0);
    }

    private boolean buyTheCar() {
        printSellCarMenu(car);
        int option = scan.nextInt();
        switch (option) {
            case 1:
                negotiate();
                break;
            case 2:
                return startTransaction(car.getInitialPrice());
        }
        return false;
    }

    private void negotiate() {
        System.out.print("What is your price?\nPrice = ");
        double proposedPrice = scan.nextDouble();
        if (proposedPrice >= car.getInitialPrice() - car.getInitialPrice() * 0.05) {
            System.out.println("The offer is good.");
            startTransaction(proposedPrice);
        } else {
            printOfferToLowMenu();
            if (scan.nextInt() == 1) {
                negotiate();
            }
        }
    }

    private boolean startTransaction(double finalPrice) {
        printFinalPriceMenu(finalPrice);
        if (scan.nextInt() == 1) {
            service.addTransaction(carMarketId, car, Status.SUCCESS, finalPrice);
            System.out.println("Done!");
            return true;
        } else {
            service.addTransaction(carMarketId, car, Status.FAILURE, finalPrice);
            System.out.println("Done!");
            return false;
        }
    }

}
