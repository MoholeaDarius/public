package javafundamentals.homework.view.util;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.model.Transaction;

import java.util.List;

public class PrintTool {

    public static void printDummyDataMenu() {
        System.out.print("Do you want to use dummy data:\n" +
                "1. Yes.\n" +
                "2. No. (you need to create form 0)\n" +
                "0. Exit.\n" +
                "Option = ");
    }

    public static void printCarMarketsMenu() {
        System.out.print("\n----------Car markets----------\n" +
                "Choose a option: \n" +
                "1. View all markets.\n" +
                "2. Add a new market.\n" +
                "3. Remove a market.\n" +
                "0. Exit.\n" +
                "Option = ");
    }

    public static void printCarMarketMenu(String name) {
        System.out.print("\n--------------" + name + "---------------\n" +
                "available options:\n" +
                "1. View all cars.\n" +
                "2. View all transactions.\n" +
                "3. Sort cars.\n" +
                "4. Add new car.\n" +
                "5. Remove car.\n" +
                "0. Back.\n" +
                "Option = ");
    }

    public static void printCarMenu(String brand, String model) {
        System.out.print("\n---------" + brand + " " + model + "---------\n" +
                "available options:\n" +
                "1. See details.\n" +
                "2. Refill.\n" +
                "3. Test drive.\n" +
                "4. Buy.\n" +
                "0. Back.\n" +
                "Option = ");
    }

    public static void printRefillMenu() {
        System.out.print("You don't have fuel, do you want to refill?\n" +
                "1. Yes.\n" +
                "2. No.\n" +
                "Option = ");
    }

    public static void printNoMoreFuelMenu(double untraveledDistance) {
        System.out.print("No more fuel.\n" +
                "You have to travel more: " + untraveledDistance + "Km. Do you want to refill?\n" +
                "1. Yes.\n" +
                "2. No.\n" +
                "Option = ");
    }

    public static void printSellCarMenu(Car car) {
        System.out.print("The car you want to buy is: " + car + "\n" +
                "The initial price is: " + car.getInitialPrice() + "$\n" +
                " Do you want to negotiate?\n" +
                "1. Yes.\n" +
                "2. No.\n" +
                "0. Back\n" +
                "Option: ");
    }

    public static void printFinalPriceMenu(double finalPrice) {
        System.out.print("The final price is : " + finalPrice + "$\n" +
                "1. Buy.\n" +
                "0. Give up.\n" +
                "Option = ");
    }

    public static void printOfferToLowMenu() {
        System.out.print("The offer is too low!\n" +
                "1. Keep negotiate.\n" +
                "0. Give up.\n" +
                "Option = ");
    }

    public static void printSortCarsMenu() {
        System.out.print("Sort cars by:\n" +
                "1. Fuel.\n" +
                "2. Mileage.\n" +
                "0. Back\n" +
                "Option = ");
    }

    public static void printAllMarkets(List<CarMarket> carMarketList) {
        for (CarMarket carMarket : carMarketList) {
            System.out.println(carMarket);
        }
    }

    public static void printAllCars(List<Car> cars) {
        if (!cars.isEmpty()) {
            for (Car car : cars) {
                System.out.println(car);
            }
            System.out.println("There are " + cars.size() + " cars in the parking.");
        } else {
            System.out.println("No cars!");
        }
    }

    public static void printAllTransactions(List<Transaction> transactions, int noOfCarsSold) {
        if (!transactions.isEmpty()) {
            for (Transaction t : transactions) {
                System.out.println(t);
            }
            System.out.println("There are " + noOfCarsSold + " cars sold.");

        } else {
            System.out.println("No Transactions!");
        }
    }

}
