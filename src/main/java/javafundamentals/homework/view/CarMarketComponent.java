package javafundamentals.homework.view;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.model.constants.CarType;
import javafundamentals.homework.service.Service;

import java.util.List;

import static javafundamentals.homework.view.util.PrintTool.*;
import static javafundamentals.homework.view.util.Scanner.scan;

public class CarMarketComponent {

    private final Service service;
    private final CarMarket carMarket;

    public CarMarketComponent(Service service, CarMarket carMarket) {
        this.service = service;
        this.carMarket = carMarket;
    }

    public void carMenu() {
        while (true) {
            printCarMarketMenu(carMarket.getName());
            int option = scan.nextInt();
            switch (option) {
                case 1:
                    printAllCars(carMarket.getCarList());
                    chooseACar();
                    break;
                case 2:
                    printAllTransactions(carMarket.getTransactions(), service.getNoOfCarsSold(carMarket.getId()));
                    break;
                case 3:
                    sort();
                    break;
                case 4:
                    addNewCar();
                    break;
                case 5:
                    removeCar();
                    break;
                case 0:
                    return;
            }
        }
    }

    private void chooseACar() {
        System.out.print("Choose a car by id.\n0. Back.\nOption = ");
        int option = scan.nextInt();
        if (option != 0) {
            CarComponents carComponents = new CarComponents(service, carMarket.getId(), service.findCarByIdAndMarketId(carMarket.getId(), option));
            carComponents.carMenu();
        }
    }

    private void sort() {
        printSortCarsMenu();
        switch (scan.nextInt()) {
            case 1:
                sortByFuel();
                break;
            case 2:
                sortByMileage();
                break;
        }
    }

    private void sortByFuel() {
        System.out.println("Enter the type of fuel.");
        CarType fuel;
        do {
            System.out.print("Fuel = ");
            fuel = CarType.fromString(scan.next());
        } while (fuel == null);
        List<Car> sortedCars = service.sortByFuel(carMarket.getId(), fuel);
        if (!sortedCars.isEmpty()) {
            for (Car car : sortedCars) {
                System.out.println(car);
            }
            chooseACar();
        }else {
            System.out.println("There are no cars with this type of fuel: " + fuel.getType());
        }
    }

    private void sortByMileage() {
        System.out.print("Enter the max mileage: ");
        int maxMileage = scan.nextInt();
        List<Car> sortedCars = service.sortByMaxMileage(carMarket.getId(), maxMileage);
        if (!sortedCars.isEmpty()) {
            for (Car car : sortedCars) {
                System.out.println(car);
            }
            chooseACar();
        }else {
            System.out.println("There are no cars with mileage under: " + maxMileage + "km");
        }

    }

    private void addNewCar() {
        if (carMarket.getCarList().size() >= carMarket.getParkingCapacity()){
            System.out.println("The parking is full!");
            return;
        }
        System.out.print("Brand = ");
        String brand = scan.next();

        System.out.print("Model = ");
        String model = scan.next();

        System.out.print("Fabrication year = ");
        int fabricationYear = scan.nextInt();

        CarType carType;
        do {
            System.out.print("Car type = ");
            carType = CarType.fromString(scan.next());
        } while (carType == null);

        System.out.print("Mileage = ");
        int mileage = scan.nextInt();

        System.out.print("Horse power = ");
        Integer horsePower = scan.nextInt();

        System.out.print("Tank volume = ");
        double tankVolume = scan.nextDouble();

        System.out.print("Fuel level = ");
        double fuelLevel = scan.nextDouble();

        System.out.print("Fuel consumption per 100Km = ");
        double fuelConsumptionPer100Km = scan.nextDouble();

        System.out.print("Initial price = ");
        double initialPrice = scan.nextDouble();

        service.addCarToCarMarket(carMarket.getId(), brand, model, fabricationYear, carType, mileage, horsePower, tankVolume, fuelLevel, fuelConsumptionPer100Km, initialPrice);
        System.out.println("Done!");
    }

    private void removeCar() {
        printAllCars(carMarket.getCarList());
        System.out.print("Choose a car by id = ");
        int carId = scan.nextInt();
        service.deleteCar(carMarket.getId(), carId);
        System.out.println("Done!");
    }

}
