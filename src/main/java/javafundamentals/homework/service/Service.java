package javafundamentals.homework.service;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.model.Transaction;
import javafundamentals.homework.model.constants.CarType;
import javafundamentals.homework.model.constants.Status;
import javafundamentals.homework.repository.Repo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Service {

    private final Repo repo;

    public Service(Repo repo) {
        this.repo = repo;
    }

    public List<CarMarket> getCarMarkets() {
        return repo.getCarMarket();
    }

    public CarMarket addCarMarket(String name, int parkingCapacity) {
        return repo.addCarMarket(new CarMarket(name, parkingCapacity, new ArrayList<>(), new ArrayList<>()));
    }

    public Car addCarToCarMarket(int carMarketId, String brand, String model, int fabricationYear, CarType carType, int mileage, Integer horsePower, double tankVolume, double fuelLevel, double fuelConsumptionPer100Km, double initialPrice) {
        return repo.addCarToCarMarket(carMarketId, new Car(brand, model, fabricationYear, carType, mileage, horsePower, tankVolume, fuelLevel, fuelConsumptionPer100Km, initialPrice));
    }

    public Transaction addTransaction(int carMarketId, Car car, Status status, double finalPrice) {
        if (status == Status.SUCCESS) {
            deleteCar(carMarketId, car.getId());
        }
        return repo.addTransactionToCarMarket(carMarketId, new Transaction(car, status, finalPrice));
    }

    public void deleteCarMarket(int marketId) {
        repo.deleteCarMarket(findCarMarketById(marketId));
    }

    public void deleteCar(int carMarketId, int carId) {
        repo.deleteCar(carMarketId, findCarByIdAndMarketId(carMarketId, carId));
    }

    public CarMarket findCarMarketById(int id) {
        return repo.findMarketById(id);
    }


    public Car findCarByIdAndMarketId(int carMarket, int carId) {
        return repo.findCarByIdAndMarketId(carMarket, carId);
    }

    public int getNoOfCarsSold(int carMarketId) {
        List<Transaction> transactions = findCarMarketById(carMarketId).getTransactions();
        return (int) transactions
                .stream()
                .filter(t -> t.getStatus().getValue().equals("success"))
                .count();
    }

    public List<Car> sortByMaxMileage(int carMarketId, int maxMileage) {
        List<Car> unsortedCars = findCarMarketById(carMarketId).getCarList();
        return unsortedCars
                .stream()
                .filter(c -> c.getMileage()<= maxMileage)
                .collect(Collectors.toList());
    }

    public List<Car> sortByFuel(int carMarketId, CarType fuel) {
        List<Car> unsortedCars = findCarMarketById(carMarketId).getCarList();
        return unsortedCars
                .stream()
                .filter(c -> c.getCarType().equals(fuel))
                .collect(Collectors.toList());
    }


}
