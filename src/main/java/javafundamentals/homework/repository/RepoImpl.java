package javafundamentals.homework.repository;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.model.Transaction;
import javafundamentals.homework.model.constants.CarType;

import java.util.ArrayList;
import java.util.List;

public class RepoImpl implements Repo {

    private final List<CarMarket> carMarkets = new ArrayList<>();

    public List<CarMarket> getCarMarket() {
        return carMarkets;
    }

    public void deleteCarMarket(CarMarket carMarket) {
        carMarkets.remove(carMarket);
    }

    public void deleteCar(int carMarketId, Car car) {
        findMarketById(carMarketId).getCarList().remove(car);
    }

    public CarMarket addCarMarket(CarMarket carMarket) {
        carMarket.setId(generateId());
        carMarkets.add(carMarket);
        return carMarket;
    }

    public Car addCarToCarMarket(int carMarketId, Car car) {
        car.setId(generateIdForCar(carMarketId));
        findMarketById(carMarketId).getCarList().add(car);
        return car;
    }

    public Transaction addTransactionToCarMarket(int carMarketId, Transaction transaction) {
        transaction.setId(generateIdForTransaction(carMarketId));
        findMarketById(carMarketId).getTransactions().add(transaction);
        return transaction;
    }

    public CarMarket findMarketById(int id) {
        for (CarMarket carMarket : carMarkets) {
            if (carMarket.getId() == id) {
                return carMarket;
            }
        }
        return null;
    }

    public Car findCarByIdAndMarketId(int carMarketId, int carId) {
        for (Car car : findMarketById(carMarketId).getCarList()) {
            if (car.getId() == carId){
                return car;
            }
        }
        return null;
    }

    private int generateId() {
        if (carMarkets.isEmpty()) {
            return 1;
        }
        return carMarkets.get(carMarkets.size() - 1).getId() + 1;
    }

    private int generateIdForCar(int marketId) {
        List<Car> cars = findMarketById(marketId).getCarList();
        if (cars.isEmpty()) {
            return 1;
        }
        return cars.get(cars.size() - 1).getId() + 1;
    }

    private int generateIdForTransaction(int marketId) {
        List<Transaction> transactions = findMarketById(marketId).getTransactions();
        if (transactions.isEmpty()) {
            return 1;
        }
        return transactions.get(transactions.size() - 1).getId() + 1;
    }

}
