package javafundamentals.homework.repository;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.model.Transaction;

import java.util.List;

public interface Repo {

    List<CarMarket> getCarMarket();

    void deleteCarMarket(CarMarket carMarket);

    void deleteCar(int carMarketId, Car car);

    CarMarket addCarMarket(CarMarket carMarket);

    Car addCarToCarMarket(int carMarketId, Car newCar);

    Transaction addTransactionToCarMarket(int carMarketId, Transaction transaction);

    CarMarket findMarketById(int id);

    Car findCarByIdAndMarketId(int carMarketId, int carId);

}
