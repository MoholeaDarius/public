package javafundamentals.homework.repository;

import javafundamentals.homework.model.Car;
import javafundamentals.homework.model.CarMarket;
import javafundamentals.homework.model.constants.CarType;

import java.util.ArrayList;
import java.util.List;

public class DataGenerator {

    public static List<CarMarket> generateDummyData() {
        List<CarMarket> carMarkets = new ArrayList<>();
        List<Car> cars = new ArrayList<>();
        cars.add(new Car(1, "Ford", "Focus", 2009, CarType.DIESEL, 129939, 109, 50, 30, 9.1, 5000));
        cars.add(new Car(2, "Audi", "a3", 2019, CarType.DIESEL, 129939, 200, 60, 21, 12, 3000));
        cars.add(new Car(3, "Volvo", "XC 60", 2010, CarType.DIESEL, 275000, 205, 50, 50, 10, 6000));
        carMarkets.add(new CarMarket(1, "AutoLand", 50, cars, new ArrayList<>()));
        List<Car> cars2 = new ArrayList<>();
        cars2.add(new Car(4, "Volvo", "XC 90 D5", 2017, CarType.ELECTRIC, 120632, 300, 80, 43, 15, 4000));
        cars2.add(new Car(5, "Volkswagen", "Golf 1.4", 2010, CarType.GASOLINE, 209000, 122, 50, 30, 8.2, 2000));
        cars2.add(new Car(6, "Citroën", "C5", 2009, CarType.DIESEL, 338000, 140, 50, 30, 9.6, 1000));
        cars2.add(new Car(7, "Hyundai", "I30", 2016, CarType.DIESEL, 196000, 110, 50, 30, 7.2, 2500));
        carMarkets.add(new CarMarket(2, "GoldenAuto", 25, cars2, new ArrayList<>()));
        return carMarkets;
    }


}
