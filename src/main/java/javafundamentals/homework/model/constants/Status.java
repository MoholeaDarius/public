package javafundamentals.homework.model.constants;

public enum Status {

    SUCCESS("success"),
    FAILURE("failure");

    String value;

    Status(String value) {
        this.value = value;
    }

    public static Status fromString(String type) {
        Status foundValue = null;
        for(Status v: Status.values()) {
            if(v.getValue().equals(type)) {
                foundValue = v;
            }
        }
        return foundValue;
    }

    public String getValue() {
        return value;
    }
}
