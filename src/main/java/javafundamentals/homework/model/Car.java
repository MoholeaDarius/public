package javafundamentals.homework.model;

import javafundamentals.homework.model.constants.CarType;

public class Car {

    private int id;
    private String brand;
    private String model;
    private int fabricationYear;
    private CarType carType;
    private int mileage;
    private Integer horsePower;
    private double tankVolume;
    private double fuelLevel;
    private double fuelConsumptionPer100Km;
    private double initialPrice;

    public Car(String brand, String model, int fabricationYear, CarType carType, int mileage, Integer horsePower, double tankVolume, double fuelLevel, double fuelConsumptionPer100Km, double initialPrice) {
        this.brand = brand;
        this.model = model;
        this.fabricationYear = fabricationYear;
        this.carType = carType;
        this.mileage = mileage;
        this.horsePower = horsePower;
        this.tankVolume = tankVolume;
        this.fuelLevel = fuelLevel;
        this.fuelConsumptionPer100Km = fuelConsumptionPer100Km;
        this.initialPrice = initialPrice;
    }

    //for dummy data
    public Car(int id, String brand, String model, int fabricationYear, CarType carType, int mileage, Integer horsePower, double tankVolume, double fuelLevel, double fuelConsumptionPer100Km, double initialPrice) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.fabricationYear = fabricationYear;
        this.carType = carType;
        this.mileage = mileage;
        this.horsePower = horsePower;
        this.tankVolume = tankVolume;
        this.fuelLevel = fuelLevel;
        this.fuelConsumptionPer100Km = fuelConsumptionPer100Km;
        this.initialPrice = initialPrice;
    }

    public boolean fillTank(double volume) {
        if (volume + fuelLevel <= tankVolume) {
            fuelLevel += volume;
            return true;
        }
        return false;
    }

    public double drive(double distance) {
        double burnedFuel = (distance * fuelConsumptionPer100Km / 100); // totalul de carburant de care am venoie sa ajung la dist
        if (fuelLevel - burnedFuel < 0) {
            double fuelNeeded = burnedFuel - fuelLevel;
            mileage += fuelLevel / fuelConsumptionPer100Km * 100;
            fuelLevel = 0.0;
            return fuelNeeded / fuelConsumptionPer100Km * 100;
        } else {
            fuelLevel -= burnedFuel;
            mileage += distance;
            return 0;
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getFabricationYear() {
        return fabricationYear;
    }

    public CarType getCarType() {
        return carType;
    }

    public int getMileage() {
        return mileage;
    }

    public Integer getHorsePower() {
        return horsePower;
    }

    public double getTankVolume() {
        return tankVolume;
    }

    public double getFuelLevel() {
        return fuelLevel;
    }

    public double getFuelConsumptionPer100Km() {
        return fuelConsumptionPer100Km;
    }


    public double getInitialPrice() {
        return initialPrice;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", fabricationYear=" + fabricationYear +
                ", carType=" + carType +
                ", mileage=" + mileage +
                ", horsePower=" + horsePower +
                ", tankVolume=" + tankVolume +
                ", fuelLevel=" + fuelLevel +
                ", fuelConsumptionPer100Km=" + fuelConsumptionPer100Km +
                ", initialPrice=" + initialPrice +
                '}';
    }
}
