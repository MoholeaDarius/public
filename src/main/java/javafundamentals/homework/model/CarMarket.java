package javafundamentals.homework.model;

import java.util.List;

public class CarMarket {

    private int id;
    private String name;
    private int parkingCapacity;
    private List<Car> carList;
    private List<Transaction> transactions;

    public CarMarket(String name, int parkingCapacity, List<Car> carList, List<Transaction> transactions) {
        this.name = name;
        this.parkingCapacity = parkingCapacity;
        this.carList = carList;
        this.transactions = transactions;
    }

    //for dummy data
    public CarMarket(int id, String name, int parkingCapacity, List<Car> carList, List<Transaction> transactions) {
        this.id = id;
        this.name = name;
        this.parkingCapacity = parkingCapacity;
        this.carList = carList;
        this.transactions = transactions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getParkingCapacity() {
        return parkingCapacity;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public String toString() {
        return "CarMarket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", parkingCapacity=" + parkingCapacity +
                '}';
    }

}
