package javafundamentals.homework.model;

import javafundamentals.homework.model.constants.Status;

public class Transaction {

    private int id;
    private Car car;
    private Status status;
    private double finalPrice;


    public Transaction(Car car, Status status, double finalPrice) {
        this.car = car;
        this.status = status;
        this.finalPrice = finalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", car=" + car +
                ", status=" + status +
                ", finalPrice=" + finalPrice +
                '}';
    }

}
